<?php

namespace App\EventSubscriber;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class JWTSubscriber implements EventSubscriberInterface
{
    public function onLexikJwtAuthenticationOnJwtCreated(JWTCreatedEvent $event)
    {
        $expiration = new \DateTime('+7 day');
        $expiration->setTime(2, 0, 0);

        $data = $event->getData();

        /**
         * @var $user User
         */
        $user = $event->getUser();
        $data["id"] = $user->getId();
        $data['username'] = $user->getUsername();
        $data['firstname'] = $user->getFirstname();
        $data['lastname'] = $user->getLastname();
        $data['kins_amount'] = $user->getKinsAmount();
        $data['bonus_amount'] = $user->getBonusAmount();
        $data['roles'] = $user->getRoles()[0]; // get first one because can have only one but keep plural method to break nothing

        $data['exp'] = $expiration->getTimestamp();

        $event->setData($data);
    }

    public static function getSubscribedEvents()
    {
        return [
            'lexik_jwt_authentication.on_jwt_created' => 'onLexikJwtAuthenticationOnJwtCreated',
        ];
    }
}
