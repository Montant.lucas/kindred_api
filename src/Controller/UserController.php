<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;


#[AsController]
class UserController extends AbstractController
{

    #[Route('/api/childrens/', name: 'app_user_childrens')]
    public function GetChildrens() : Response
    {
        /**
         * @var $user User
         */
        $user = $this->getUser();
        $childrens = $user->getChild();

        return $this->json( $childrens);
    }

    #[Route('/api/parent/', name: 'app_user_parent')]
    public function GetParent(): Response
    {
        /**
         * @var $user User
         */
        $user = $this->getUser();
        $parent = $user->getParent();

        return $this->json($parent);
    }
}
