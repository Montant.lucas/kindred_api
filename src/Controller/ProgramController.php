<?php

namespace App\Controller;

use App\Entity\Program;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[AsController]
class ProgramController extends AbstractController
{
    #[Route('/api/user-programs/', name: 'app_programs')]
    public function GetPrograms(): Response
    {
        /**
         * @var $user User
         */
        $user = $this->getUser();

        $childs = $user->getChild();

        $programs = [];

        foreach ($childs as $child) {
            $programs = array_merge($programs, $child->getPrograms()->toArray());
        }


        return $this->json($programs);
    }

    #[Route('/api/program-tasks/{id}', name: 'app_program_tasks')]
    public function GetTasks($id, ManagerRegistry $doctrine): Response
    {
        /**
         * @var Program $program
         */
        $program = $doctrine->getRepository(Program::class)->find($id);

        if (!$program) {
            throw $this->createNotFoundException(
                'No product found for id '. $id
            );
        }
        return $this->json($program->getTasks());
    }

}