<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    #[Route('/api/login', name: 'api_login', methods: ['POST'])]
    public function apiLoginCheck(): Response
    {
        /**
         * @var $user User
         */
        $user = $this->getUser();
        return $this->json([
            "id" => $user->getId(),
            'username'  => $user->getUsername(),
            'firstname'  => $user->getFirstname(),
            'lastname'  => $user->getLastname(),
            'kins_amount'  => $user->getKinsAmount(),
            'bonus_amount'  => $user->getBonusAmount(),
            'roles' => $user->getRoles()[0], // get first one because can have only one but keep plural method to break nothing
        ]);
    }
}
