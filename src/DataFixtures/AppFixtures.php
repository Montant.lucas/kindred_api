<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $userPasswordHasher;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        $parent = new User();
        $parent->setUsername('parent');
        $parent->setPassword($this->userPasswordHasher->hashPassword($parent, 'parent'));
        $manager->persist($parent);

        $child = new User();
        $child->setUsername('child');
        $child->setParent($parent);
        $child->setPassword($this->userPasswordHasher->hashPassword($child, 'parent'));
        $manager->persist($child);

        $manager->flush();
    }
}
