<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ProgramController;
use App\Repository\ProgramRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ProgramRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post',
    ],
    itemOperations: [
        'get',
        'put',
        'delete',
        'patch',
        'app_programs' => [
            'method' => 'GET',
            'route_name' => 'app_programs',
            'path'      => "/api/user-programs/",
            'controller' => ProgramController::class,
        ],
        'app_program_tasks' => [
            'method' => 'GET',
            'route_name' => 'app_program_tasks',
            'path'      => "/api/program-tasks/{id}",
            'controller' => ProgramController::class,
        ],
    ],
)]
class Program
{
    #[Groups("program")]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $userId;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'programs')]
    private $user;

    #[ORM\Column(type: 'date', nullable: true)]
    private $start_date;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $reward;

    #[ORM\OneToMany(mappedBy: 'program', targetEntity: Task::class)]
    private $tasks;

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(?int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(?\DateTimeInterface $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getReward(): ?int
    {
        return $this->reward;
    }

    public function setReward(?int $reward): self
    {
        $this->reward = $reward;

        return $this;
    }

    /**
     * @return Collection<int, Task>
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setProgram($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->tasks->removeElement($task)) {
            // set the owning side to null (unless already changed)
            if ($task->getProgram() === $this) {
                $task->setProgram(null);
            }
        }

        return $this;
    }
}
